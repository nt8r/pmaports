# Maintainer: Oliver Smith <ollieparanoid@postmarketos.org>
# Co-Maintainer: Clayton Craft <clayton@craftyguy.net>
pkgname=postmarketos-initramfs
pkgver=1.2.0
pkgrel=0
pkgdesc="Base files for the postmarketOS initramfs / initramfs-extra"
url="https://postmarketos.org"
depends="
	btrfs-progs
	busybox-extras
	bzip2
	cryptsetup
	device-mapper
	devicepkg-utils
	e2fsprogs
	e2fsprogs-extra
	f2fs-tools
	lz4
	multipath-tools
	parted
	postmarketos-fde-unlocker
	postmarketos-mkinitfs
	unudhcpd
	xz
	"
source="
	00-default.modules
	00-initramfs-base.dirs
	00-initramfs-base.files
	00-initramfs-extra-base.files
	init.sh
	init_functions.sh
	"
arch="noarch"
license="GPL-2.0-or-later"

package() {
	install -Dm644 "$srcdir/init_functions.sh" \
		"$pkgdir/usr/share/mkinitfs/init_functions.sh"

	install -Dm755 "$srcdir/init.sh" \
		"$pkgdir/usr/share/mkinitfs/init.sh"

	install -Dm644 "$srcdir"/00-initramfs-base.dirs \
		-t "$pkgdir"/usr/share/mkinitfs/dirs/
	mkdir -p "$pkgdir"/etc/mkinitfs/dirs

	install -Dm644 "$srcdir"/00-default.modules \
		-t "$pkgdir"/usr/share/mkinitfs/modules/
	mkdir -p "$pkgdir"/etc/mkinitfs/modules

	install -Dm644 "$srcdir"/00-initramfs-base.files \
		-t "$pkgdir"/usr/share/mkinitfs/files/
	mkdir -p "$pkgdir"/etc/mkinitfs/files

	install -Dm644 "$srcdir"/00-initramfs-extra-base.files \
		-t "$pkgdir"/usr/share/mkinitfs/files-extra/
	mkdir -p "$pkgdir"/etc/mkinitfs/files-extra

	mkdir -p "$pkgdir"/usr/share/mkinitfs/hooks
	mkdir -p "$pkgdir"/usr/share/mkinitfs/hooks-extra
	mkdir -p "$pkgdir"/etc/mkinitfs/hooks
	mkdir -p "$pkgdir"/etc/mkinitfs/hooks-extra
}

sha512sums="
bed319179bcd0b894d6267c7e73f2890db07bc07df71542936947dfb3bdb17fade8a7b4e7b577f278af4472464427bba07f75aff0a1e454a4167052c088f3b6a  00-default.modules
399da6e61993f48c8a62c956bb15d294cac10bf003d84257efdf4a213ebc87bb51cdcd75c4675f51c3be832146b8f21a7c769bf3e94f574a5067f001662632a1  00-initramfs-base.dirs
3bc73f3a0d1de210444d9c45fab51fd4520e38b43ffbb76368f8ff854b990aa2f21c6356840c5a48eb9808e834eb885a6d581639a60707c26abf66fc20b70db9  00-initramfs-base.files
e984cd3033ce8752ebc71127828b964b46259a5263c2ebfab32c1394b674bcff464862ff00b8e920d3d31386c54ca0b94f84bc77580d275ecfeea33e76c07ef4  00-initramfs-extra-base.files
5a6252a02f46a4649a5aa7731a3f1bffd6eeca26b36bfd7ba1920dd6c72cceecfe0101da20d2f53850f1394a23f2dc1a61bfcfcf9c99ecdd26aec26df9a72d4e  init.sh
a6358ca43f36d84d64da063db456fa7520eeb7bbdb457082e6959995166156245b08d9e7c1098b6f2f52974e7cbcc0b7719e4a0d3400a1d989c9e7b831303bb4  init_functions.sh
"
